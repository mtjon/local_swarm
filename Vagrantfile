API_VERSION = "2"
VAGRANT_BOX = "ubuntu/bionic64"

MANAGERS = 1 # Require at least one
WORKERS = 1

MANAGER_CPUS = 1
MANAGER_MEMORY = 512

WORKER_CPUS = 2
WORKER_MEMORY = 1024

ANSIBLE_GROUPS = {
  "managers" => ["manager[1:#{MANAGERS}]"],
  "leader" => ["manager1"],
  "workers" => ["worker[1:#{WORKERS}]"],
  "ubuntu18_hosts:children" => ["managers",
                                "workers"],
  "all_groups:children" => ["managers",
                            "workers"],
  "py3_hosts:children" => ["ubuntu18_hosts"],
  "py3_hosts:vars" => {"ansible_python_interpreter" => "/usr/bin/python3"}
}

Vagrant.configure(API_VERSION) do |config|

    config.vm.box = VAGRANT_BOX

    (1..MANAGERS).each do |manager_id|
        config.vm.define "manager#{manager_id}" do |manager|
            manager.vm.define "manager#{manager_id}"
            manager.vm.network "private_network", ip: "10.3.2.#{1+manager_id}"
            manager.vm.provider "virtualbox" do |v|
                v.cpus = MANAGER_CPUS
                v.memory = MANAGER_MEMORY
            end

            manager.vm.provision :ansible do |ansible|
                ansible.playbook = "ansible/docker.yml"
                ansible.groups = ANSIBLE_GROUPS
            end

            if WORKERS == 0 and manager_id == MANAGERS
                manager.vm.provision :ansible do |ansible|
                    ansible.limit = "all"
                    ansible.playbook = "ansible/swarm_module.yml"
                    ansible.groups = ANSIBLE_GROUPS
                end
            end
        end
    end

    (1..WORKERS).each do |worker_id|
        config.vm.define "worker#{worker_id}" do |worker|
            worker.vm.define "worker#{worker_id}"
            worker.vm.network "private_network", ip: "10.3.2.#{1+MANAGERS+worker_id}"
            worker.vm.provider "virtualbox" do |v|
                v.cpus = WORKER_CPUS
                v.memory = WORKER_MEMORY
            end

            worker.vm.provision :ansible do |ansible|
                ansible.playbook = "ansible/docker.yml"
                ansible.groups = ANSIBLE_GROUPS
            end

            if worker_id == WORKERS
                worker.vm.provision :ansible do |ansible|
                    ansible.limit = "all"
                    ansible.playbook = "ansible/swarm_module.yml"
                    ansible.groups = ANSIBLE_GROUPS
                end
            end
        end
    end
end
